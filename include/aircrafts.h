/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Structure for aircrafts
*/

#include <SFML/Graphics.h>

#ifndef PLANE_STRUCT_H_
#define PLANE_STRUCT_H_

typedef struct plane
{
	sfVector2f depart;
	sfVector2f arrival;
	sfVector2f speed;
	int	time;
	sfSprite *plane_sprite;
	sfRectangleShape *hitbox;
	struct plane *next;
	struct plane *prev;
}plane_t;

#endif /* PLANE_STRUCT_H_ */
