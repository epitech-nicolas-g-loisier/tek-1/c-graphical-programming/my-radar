/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Functions Prototypes for My Radar
*/

#include <SFML/Graphics.h>
#include "aircrafts.h"
#include "tower.h"
#include "danger.h"
#include "data.h"

/* USEFULL FUNCTION */
int	count_arg(char*);
int	count_digit(int);
int	my_get_nbr(char*);
int	my_get_percent(char*);
int	my_strcmp(char const*, char const*);

/* display_window.c */
sfRenderWindow	*my_window_create(void);
void	open_window(data_t*);

/* get_danger_data.c */
sfVector2f get_danger_pos(char*, int*);
danger_t *get_danger_data(char*);

/* get_data.c */
data_t	*get_data(char*);

/* get_plane_dat.c */
sfVector2f get_plane_pos(char*, int*);
plane_t	*get_plane_data(char*);

/* get_tower_data.c */
sfVector2f      get_tower_pos(char*, int*);
tower_t *get_tower_data(char*);

/* main.c */
void	display_help(void);
void	check_arg(char*);
