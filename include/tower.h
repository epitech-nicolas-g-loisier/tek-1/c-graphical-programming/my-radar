/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Structure for tower
*/

#include <SFML/Graphics.h>

#ifndef TOWER_STRUCT_H_
#define TOWER_STRUCT_H_

typedef struct tower
{
	sfVector2f coord;
	int	radius;
	sfSprite *tower_sprite;
	sfCircleShape *zone;
	struct tower *next;
}tower_t;

#endif /* TOWER_STRUCT_H_ */
