/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Structure for all data
*/

#include <SFML/Graphics.h>
#include "aircrafts.h"
#include "tower.h"
#include "danger.h"

#ifndef DATA_STRUCT_H_
#define DATA_STRUCT_H_

typedef struct data
{
	plane_t *planes;
	tower_t *towers;
	danger_t *dangers;
}data_t;

#endif /* DATA_STRUCT_H_ */
