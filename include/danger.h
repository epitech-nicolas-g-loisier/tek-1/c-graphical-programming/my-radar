/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Structure for danger area
*/

#include <SFML/Graphics.h>

#ifndef DANGER_STRUCT_H_
#define DANGER_STRUCT_H_

typedef struct danger
{
	sfVector2f *coord;
	sfConvexShape *zone;
	struct danger *next;
}danger_t;

#endif /* DANGER_STRUCT_H_ */
