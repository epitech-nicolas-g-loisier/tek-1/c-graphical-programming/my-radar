/*
** EPITECH PROJECT, 2018
** Get Next Line
** File description:
** Get Next Line
*/

#ifndef GET_NEXT_LINE_H
#define GET_NEXT_LINE_H

#ifndef READ_SIZE
#define READ_SIZE (10)
#endif /* READ_SIZE */

char	*get_next_line(int fd);

#endif /* GET_NEXT_LINE_H */
