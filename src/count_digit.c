/*
** EPITECH PROJECT, 2017
** Utilities
** File description:
** count number of digit of an int
*/

int	count_digit(int	nb)
{
	int	count = 0;

	if (nb == 0)
		return 1;
	while (nb != 0){
		nb = nb / 10;
		count++;
	}
	count++;
	return count;
}
