 /*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Count number of arguments in a string
*/

int	count_arg(char *str)
{
	int	count = 0;
	int	count_arg = 0;

	while (str[count] != '\0'){
		if (str[count] == ' ' || str[count] == '\t'){
			count_arg++;
		}
		while (str[count] == ' ' || str[count] == '\t'){
			count++;
		}
		count++;
	}
	return (count_arg);
}
