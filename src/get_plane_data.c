/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Get data for airplanes from a string
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <SFML/Graphics.h>
#include "my_radar.h"
#include "aircrafts.h"

sfVector2f	get_plane_pos(char *line, int *count)
{
	sfVector2f	coord;
	int	x = 0;
	int	y = 0;

	x = my_get_percent(&line[*count]);
	*count += count_digit(x);
	y = my_get_percent(&line[*count]);
	*count += count_digit(y);
	coord.x = x * 8;
	coord.y = y * 6;
	return coord;
}

sfRectangleShape *create_hitbox(sfVector2f coord)
{
	sfRectangleShape *hitbox = sfRectangleShape_create();

	sfRectangleShape_setSize(hitbox, (sfVector2f){20, 20});
	sfRectangleShape_setPosition(hitbox, coord);
	sfRectangleShape_setFillColor(hitbox, sfTransparent);
	sfRectangleShape_setOutlineColor(hitbox, sfYellow);
	sfRectangleShape_setOutlineThickness(hitbox, 1);
	return (hitbox);
}

void	get_speed(double speed, plane_t *plane)
{
	double	x = plane->arrival.x - plane->depart.x;
	double	y = plane->arrival.y - plane->depart.y;
	double	hypo = sqrt(pow((x), 2) + pow((y), 2));
	double	angle = 0;
	static int i = 0;

	speed = speed * 8;
	plane->speed.x = x * (speed / hypo) / 30;
	plane->speed.y = y * (speed / hypo) / 30;
	if (x >= 0 && y < 0)
		angle = 180 - (acos(y / hypo) * (180.0 / M_PI));
	else if (x < 0 && y < 0)
		angle = 90 - (acos(y / hypo) * (180.0 / M_PI));
	else {
		if (x < 0 && y >= 0)
			angle = -180 + (acos(y / hypo) * (180.0 / M_PI));
		else
			angle = 180 + (acos(y / hypo) * (180.0 / M_PI));
	}
	i++;
	sfSprite_setRotation(plane->plane_sprite, angle);
	sfRectangleShape_setRotation(plane->hitbox, angle);
}

plane_t	*get_plane_data(char *line)
{
	plane_t *new_plane = malloc(sizeof(plane_t));
	int	count = 0;
	int	nb_arg = count_arg(line);
	double	speed_value = 0;
	sfTexture *plane = sfTexture_createFromFile("sprite/airplane.png"
							, NULL);
	new_plane->plane_sprite = sfSprite_create();
	sfSprite_setTexture(new_plane->plane_sprite, plane, sfTrue);
	if (nb_arg != 5)
		exit(84);
	new_plane->depart = get_plane_pos(line, &count);
	sfSprite_setPosition(new_plane->plane_sprite, new_plane->depart);
	new_plane->hitbox = create_hitbox(new_plane->depart);
	new_plane->arrival = get_plane_pos(line, &count);
	speed_value = my_get_percent(&line[count]);
	get_speed(speed_value, new_plane);
	count += count_digit(speed_value);
	new_plane->time = my_get_nbr(&line[count]);
	new_plane->next = NULL;
	return (new_plane);
}
