/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Get data for towers from a string
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <SFML/Graphics.h>
#include "my_radar.h"
#include "tower.h"

sfVector2f	get_tower_pos(char *line, int *count)
{
	sfVector2f	coord;
	int	x = 0;
	int	y = 0;

	x = my_get_percent(&line[*count]);
	*count += count_digit(x);
	y = my_get_percent(&line[*count]);
	*count += count_digit(y);
	coord.x = x * 8;
	coord.y = y * 6;
	return coord;
}

sfCircleShape *create_zone(sfVector2f coord, int radius)
{
	sfCircleShape *zone = sfCircleShape_create();

	coord.x -= radius - 25;
	coord.y -= radius - 25;
	sfCircleShape_setRadius(zone, radius);
	sfCircleShape_setPosition(zone, coord);
	sfCircleShape_setFillColor(zone, sfTransparent);
	sfCircleShape_setOutlineColor(zone, sfBlue);
	sfCircleShape_setOutlineThickness(zone, 2.5);
	return (zone);
}

tower_t	*get_tower_data(char *line)
{
	tower_t	*new_tower = malloc(sizeof(tower_t));
	int	count = 0;
	int	nb_arg = count_arg(line);
	sfTexture *tower = sfTexture_createFromFile("sprite/controle_tower.png"
							, NULL);
	sfVector2f coord;

	new_tower->tower_sprite = sfSprite_create();
	sfSprite_setTexture(new_tower->tower_sprite, tower, sfTrue);
	if (nb_arg != 2) {
		write(2, "invalid number of argument for tower\n", 37);
		exit(84);
	}
	coord = get_tower_pos(line, &count);
	sfSprite_setPosition(new_tower->tower_sprite, coord);
	new_tower->radius = my_get_percent(&line[count]) * 8;
	new_tower->zone = create_zone(coord, new_tower->radius);
	new_tower->next = NULL;
	return (new_tower);
}
