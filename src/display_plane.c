/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Display planes and move them
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <SFML/Graphics.h>
#include "my_radar.h"
#include "data.h"

int	check_if_arrive(sfVector2f pos, sfVector2f arrival, sfVector2f speed)
{
	double	x = arrival.x - pos.x;
	double	y = arrival.y - pos.y;

	if ((x * (x + speed.x)) < 0)
		return (1);
	else if ((y * (y + speed.y)) < 0)
		return (1);
	else
		return (0);
}

plane_t	*move_plane(plane_t *planes, sfVector2f pos)
{
	pos.x += planes->speed.x;
	pos.y += planes->speed.y;
	sfSprite_setPosition(planes->plane_sprite, pos);
	sfRectangleShape_setPosition(planes->hitbox, pos);
	planes = planes->next;
	return planes;
}

plane_t	*check_move_planes(plane_t *planes, sfClock *clock)
{
	plane_t	*tmp = planes;
	plane_t	*prev = NULL;
	sfTime	time = sfClock_getElapsedTime(clock);
	sfVector2f pos;

	while (tmp != NULL){
		pos = sfSprite_getPosition(tmp->plane_sprite);
		if (check_if_arrive(pos, tmp->arrival, tmp->speed) == 1){
			if (prev == NULL){
				planes = tmp->next;
				tmp = planes;
			} else {
				prev->next = tmp->next;
				tmp = prev->next;
			}
		} else if ((time.microseconds / 1000000) >= tmp->time){
			prev = tmp;
			tmp = move_plane(tmp, pos);
		} else {
			prev = tmp;
			tmp = tmp->next;
		}
	}
	return planes;
}

void	draw_plane(plane_t *planes, sfRenderWindow *window, sfClock *clock)
{
	plane_t	*tmp = planes;
	sfTime	time = sfClock_getElapsedTime(clock);

	while (tmp != NULL){
		if ((time.microseconds / 1000000) >= tmp->time){
			sfRenderWindow_drawSprite(window, tmp->plane_sprite
						, NULL);
			sfRenderWindow_drawRectangleShape(window, tmp->hitbox
							, NULL);
		}
		tmp = tmp->next;
	}
}
