/*
** EPITECH PROJECT, 2018
** Get Next Line
** File description:
** Returns a read line from a file descriptor
*/

#include "get_next_line.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

char	*init_str(int size)
{
	char	*str = malloc(sizeof(char) * size + 1);

	str[0] = '\0';
	return str;
}

int	length(char const *str, char stop)
{
	int	i = 0;

	if (str != NULL){
		while (str[i] != '\0' && str[i] != stop)
			i++;
	}
	return (i);
}

char	*add_str(char *line, char *add, int stop)
{
	char	*dest = init_str(stop + length(line, '\0'));
	int	count = 0;
	int	counter = 0;

	if (line != NULL){
		while (line[count] != '\0'){
			dest[count] = line[count];
			count++;
		}
		free(line);
	}
	if (add != NULL){
		while (add[counter] != '\0' && counter < stop){
			dest[count] = add[counter];
			count++;
			counter++;
		}
	}
	dest[count] = '\0';
	return dest;
}

char	*read_line_and_keep_remaining(int fd, int n)
{
	static char* keep = NULL;
	char	*buffer = init_str(n);
	char	*line = init_str(n);
	int	size = n;
	int	count = 0;

	if (keep != NULL)
		line = add_str(keep, NULL, 0);
	while (buffer[count] != '\n' && size == n){
		count = 0;
		if ((size = read(fd, buffer, n)) <= 0 && line[0] == '\0'){
			free(line);
			free(buffer);
			return NULL;
		}
		buffer[size] = '\0';
		count = length(buffer, '\n');
		line = add_str(line, buffer, count);
	}
	if (size > 0 && buffer[count] == '\n')
		keep = add_str(NULL, &buffer[(count + 1)], n);
	else
		keep = NULL;
	free(buffer);
	return (line);
}

char	*get_next_line(int fd)
{
	static char* store = NULL;
	int	count = 0;
	char	*line = store;
	char	*ret = NULL;

	if (fd == -1 || READ_SIZE < 1 || READ_SIZE > INT_MAX)
		return NULL;
	else if (store == NULL || store[0] == '\0')
		line = read_line_and_keep_remaining(fd, READ_SIZE);
	while (line != NULL && line[count] != '\0'){
		if (line[count] == '\n'){
			store = add_str(NULL, &line[(count + 1)], READ_SIZE);
			ret = add_str(NULL, line, count);
			free(line);
			return ret;
		}
		count++;
	}
	store = NULL;
	if (line == NULL)
		return NULL;
	else
		return line;
}
