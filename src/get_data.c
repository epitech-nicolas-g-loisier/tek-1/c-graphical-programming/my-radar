/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** get data from the texte file
*/

#include "my_radar.h"
#include "get_next_line.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

plane_t *add_planes(char *line, plane_t *planes)
{
	plane_t *tmp = planes;

	if (tmp != NULL){
		while (tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = get_plane_data(&line[1]);
		tmp->next->prev = tmp->next;
	}
	else {
		planes = get_plane_data(&line[1]);
		planes->prev = NULL;
	}
	return planes;
}

tower_t *add_towers(char *line, tower_t *towers)
{
	tower_t *tmp = towers;

	if (tmp != NULL){
		while (tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = get_tower_data(&line[1]);
	}
	else {
		towers = get_tower_data(&line[1]);
	}
	return towers;
}

danger_t *add_dangers(char *line, danger_t *dangers)
{
	danger_t *tmp = dangers;

	if (tmp != NULL){
		while (tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = get_danger_data(&line[1]);
	}
	else {
		dangers = get_danger_data(&line[1]);
	}
	return dangers;
}

data_t	*get_data(char *filepath)
{
	char	*line = NULL;
	int	fd = open(filepath, O_RDONLY);
	data_t	*data = malloc(sizeof(data_t*));

	data->towers = NULL;
	data->planes = NULL;
	line = get_next_line(fd);
	while (line != NULL){
		if (line[0] == 'A'){
			data->planes = add_planes(&line[1], data->planes);
		} else if (line[0] == 'T'){
			data->towers = add_towers(&line[1], data->towers);
		} else {
			if (line[0] == 'D'){
				data->dangers = add_dangers(&line[1], data->dangers);
			} else {
				write(2, "file is invalid\n", 16);
				exit(84);
			}
		}
		line = get_next_line(fd);
	}
	close(fd);
	return (data);
}
