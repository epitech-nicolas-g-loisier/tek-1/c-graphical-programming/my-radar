/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** simulatation of air traffic
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "my_radar.h"

void	display_help(void)
{
	write(1, "Air traffic simulation panel\n", 29);
	write(1, "USAGE\n", 6);
	write(1, "    ./my_radar [OPTIONS] path_to_script\n", 40);
	write(1, "    path_to_script   The path to the script file.\n", 50);
	write(1, "OPTIONS\n", 8);
	write(1, "    -h print the usage and quit.\n", 33);
}

void	check_arg(char	*arg)
{
	if (my_strcmp(arg, "-h") == 0){
		display_help();
		exit(0);
	} else if (open(arg, O_RDONLY) == -1){
		write(2, "invalid file name\n", 18);
		exit(84);
	}
}

int	main(int argc, char **argv)
{
	data_t *data = malloc(sizeof(data_t*));

	if (argc != 2){
		write(2, "./my_radar: bad arguments: ", 27);
		write(2, "1 argument is required", 22);
		write(2, "\nretry with -h\n", 15);
		return (84);
	} else {
		check_arg(argv[1]);
		data = get_data(argv[1]);
		open_window(data);
		return 0;
	}
}
