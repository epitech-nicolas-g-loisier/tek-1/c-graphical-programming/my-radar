/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** compare two strings
*/

#include <stdlib.h>
#include <stdio.h>
#include "my_radar.h"

int my_strcmp(char const *s1, char const *s2)
{
	int	i = 0;
	int	ret = 0;

	while (s1[i] != '\0' && s2[i] != '\0'){
		if (s1[i] == s2[i]){
			i++;
		} else if (s1[i] < s2[i]){
			ret = -1;
			break;
		} else {
			ret = 1;
			break;
		}
	}
	if (ret == -1 || (s1[i] == 0 && s2[i] != 0))
		return -1;
	else if (ret == 1 || (s1[i] != 0 && s2[i] == 0))
		return 1;
	else
		return 0;
}
