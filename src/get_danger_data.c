/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** Get data for dangers from a string
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <SFML/Graphics.h>
#include "my_radar.h"
#include "danger.h"

sfVector2f	get_danger_pos(char *line, int *count)
{
	sfVector2f	coord;
	int	x = 0;
	int	y = 0;

	x = my_get_percent(&line[*count]);
	*count += count_digit(x);
	y = my_get_percent(&line[*count]);
	*count += count_digit(y);
	coord.x = x * 8;
	coord.y = y * 6;
	return coord;
}

sfConvexShape *create_convex_shape(size_t pos, sfVector2f *coord)
{
	size_t	i = 0;
	sfConvexShape *zone = sfConvexShape_create();
	sfTexture *texture = sfTexture_createFromFile("sprite/danger_zone.png"
							, NULL);

	sfConvexShape_setPointCount(zone, pos);
	while (coord[i].x != -1){
		sfConvexShape_setPoint(zone, i , coord[i]);
		i++;
	}
	sfConvexShape_setFillColor(zone, sfTransparent);
	sfConvexShape_setOutlineColor(zone, sfRed);
	sfConvexShape_setOutlineThickness(zone, 2.5);
	sfConvexShape_setTexture(zone, texture, sfTrue);
	return (zone);
}

danger_t *get_danger_data(char *line)
{
	danger_t *new_danger = malloc(sizeof(danger_t));
	int	count = 0;
	int	pos = 0;
	int	nb_arg = count_arg(line);

	if ((nb_arg % 2) != 1){
		write(2, "invalid coord for danger zone\n", 30);
		exit(84);
	}
	new_danger->coord = malloc(sizeof(sfVector2f) * (nb_arg / 2 + 1));
	while (pos != (nb_arg / 2 + 1)){
		new_danger->coord[pos] = get_danger_pos(line, &count);
		pos++;
	}
	new_danger->coord[pos].x = -1;
	new_danger->coord[pos].y = -1;
	new_danger->zone = create_convex_shape(pos, new_danger->coord);
	return (new_danger);
}
