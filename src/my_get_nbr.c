/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** get positive number
*/

#include "get_next_line.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "my_radar.h"

int	my_get_nbr(char *str)
{
	int	nb = 0;
	int	count = 0;

	while (str[count] == ' ' || str[count] == '\t')
		count++;
	while (str[count] >= '0' && str[count] <= '9'){
		nb = nb * 10;
		nb += str[count] - '0';
		count++;
	}
	if (str[count] != ' ' && str[count] != '\t'
			&& str[count] != '\0'){
		write(2, "invalid file\n", 13);
		exit(84);
	} else
		return nb;
}

int	my_get_percent(char *str)
{
	int	nb = 0;
	int	count = 0;

	while (str[count] == ' ' || str[count] == '\t')
		count++;
	while (str[count] >= '0' && str[count] <= '9'){
		nb = nb * 10;
		nb += str[count] - '0';
		count++;
	}
	if (nb > 100){
		write(2, "percent > 100\n", 15);
		exit(84);
	} else if (str[count] != ' ' && str[count] != '\t'
			&& str[count] != '\0'){
		write(2, "invalid file\n", 13);
		exit(84);
	} else
		return nb;
}
