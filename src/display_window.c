/*
** EPITECH PROJECT, 2018
** My Radar
** File description:
** display window
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <SFML/Graphics.h>
#include "my_radar.h"
#include "data.h"

sfRenderWindow *my_window_create(void)
{
	sfRenderWindow	*window;
	sfVideoMode	mode;

	mode.width = 800;
	mode.height = 600;
	mode.bitsPerPixel = 32;
	window = sfRenderWindow_create(mode, "My Radar", sfDefaultStyle, NULL);
	sfRenderWindow_setFramerateLimit(window, 30);
	return (window);
}

void	draw_tower(tower_t *towers, sfRenderWindow *window)
{
	tower_t *tmp = towers;
	sfCircleShape *tmp_circle;
	sfVector2f tmp_coord;

	while (tmp != NULL){
		tmp_circle = sfCircleShape_copy(tmp->zone);
		tmp_coord = sfCircleShape_getPosition(tmp->zone);
		sfRenderWindow_drawSprite(window, tmp->tower_sprite, NULL);
		sfRenderWindow_drawCircleShape(window, tmp->zone, NULL);
		if ((tmp_coord.x + 2 * tmp->radius + 25) > 800){
			tmp_coord.x -= 800;
		}
		sfCircleShape_setPosition(tmp_circle, tmp_coord);
		sfRenderWindow_drawCircleShape(window, tmp_circle, NULL);
		tmp = tmp->next;
	}
}

void	check_if_close(plane_t *planes, sfRenderWindow *window, sfEvent event)
{
	if (planes == NULL)
		sfRenderWindow_close(window);
	while (sfRenderWindow_pollEvent(window, &event)){
		if (event.type == sfEvtClosed)
			sfRenderWindow_close(window);
	}
}

sfSprite *create_background(void)
{
	sfTexture	*texture;
	sfSprite	*sprite = sfSprite_create();

	texture = sfTexture_createFromFile("sprite/map.png", NULL);
	sfSprite_setTexture(sprite, texture, sfTrue);
	return (sprite);
}

void	open_window(data_t *data)
{
	sfRenderWindow	*window = my_window_create();
	sfSprite	*sprite = create_background();
	sfClock	*clock = sfClock_create();
	sfEvent	event;

	while (sfRenderWindow_isOpen(window)) {
		check_if_close(data->planes, window, event);
		sfRenderWindow_clear(window, sfBlack);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		if (data->dangers != NULL)
			sfRenderWindow_drawConvexShape(window,
						       data->dangers->zone
						       , NULL);
		draw_tower(data->towers, window);
		draw_plane(data->planes, window, clock);
		data->planes = check_move_planes(data->planes, clock);
		sfRenderWindow_display(window);
	}
	sfSprite_destroy(sprite);
	sfRenderWindow_destroy(window);
}
