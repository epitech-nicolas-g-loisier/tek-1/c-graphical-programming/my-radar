##
## EPITECH PROJECT, 2017
## 
## File description:
## Makefile
##

SRC_DIR	=	$(realpath src)

CFLAGS	=	-Wall -Wextra -Iinclude/

SRC	=	$(SRC_DIR)/main.c	\
		$(SRC_DIR)/my_strcmp.c	\
		$(SRC_DIR)/display_window.c	\
		$(SRC_DIR)/get_data.c	\
		$(SRC_DIR)/get_next_line.c	\
		$(SRC_DIR)/my_get_nbr.c	\
		$(SRC_DIR)/count_digit.c	\
		$(SRC_DIR)/get_plane_data.c	\
		$(SRC_DIR)/get_tower_data.c	\
		$(SRC_DIR)/get_danger_data.c	\
		$(SRC_DIR)/count_arg.c	\
		$(SRC_DIR)/display_plane.c

OBJ	=	$(SRC:.c=.o)

NAME	=	my_radar

all:            $(NAME)

$(NAME):	$(OBJ)
		gcc -o $(NAME) $(OBJ) $(CFLAGS) -l c_graph_prog -lm

clean:
		rm -f $(OBJ)

fclean:		clean
		rm -f $(NAME)

re:		fclean all
